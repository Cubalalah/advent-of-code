input = open("2022/03/Input.txt","r")
lines = input.readlines()
ruckpacks = []
letter = 0
sum = 0

def priority(letter):
    if ord(str(letter))>96:
        return ord(str(letter))-96
    else:
        return ord(str(letter))-38

for i in lines:
    i = i[:-1]
    ruckpacks.append(i)
for sac in ruckpacks:
    middle=len(sac)//2
    first_half=sac[:middle]
    second_half=sac[middle:]
    for item1 in first_half:
        if item1 in second_half:
            sum += priority(item1)
            break

print("Part 1 : The sum of the priorities of the failed items is", sum)

sum = 0
for i in range(0,int(len(ruckpacks)),3):
    for chr1 in ruckpacks[i]:
        if chr1 in ruckpacks[i+1] and chr1 in ruckpacks[i+2]:
            sum+=priority(chr1)
            break

print("Part 2 : The sum of the priorities of the badges is", sum)