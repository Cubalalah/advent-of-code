input = open("2022/18/Input.txt", "r")
lines = input.read().splitlines()
all_pos = set()
count = 0

for line in lines:
    line = [int(x) for x in line.split(",")]
    pos = (line[0],line[1],line[2])
    all_pos.add(pos)

all_neighbour = [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]

for pos in all_pos:
    for neighbour in all_neighbour:
        if (pos[0]+neighbour[0],pos[1]+neighbour[1],pos[2]+neighbour[2]) in all_pos:
                    count += 1
                    
surface = 6*len(all_pos)-count
print("Part 1 :",surface)

