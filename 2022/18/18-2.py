input = open("2022/18/Input.txt", "r")
lines = input.read().splitlines()
all_pos = set()
count = 0
neighbours = set()

for line in lines:
    line = [int(x) for x in line.split(",")]
    pos = (line[0],line[1],line[2])
    all_pos.add(pos)

all_neighbour = [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]
outside = set()
inside = set()

def searchingExit(pos,testing_set):
    for neighbour in all_neighbour:
        new_pos = (pos[0]+neighbour[0],pos[1]+neighbour[1],pos[2]+neighbour[2])
        if new_pos in outside:
            return True
        if new_pos in inside:
            return False
        if new_pos not in all_pos and new_pos not in testing_set:
            testing_set.add(new_pos)
            if new_pos[0]<0 or new_pos[0]>20 or new_pos[1]<0 or new_pos[1]>20 or new_pos[2]<0 or new_pos[2]>20:
                return True
            res = searchingExit(new_pos,testing_set)
            if res != None:
                return res
    return None

for pos in all_pos:
    for neighbour in all_neighbour:
        new_pos = (pos[0]+neighbour[0],pos[1]+neighbour[1],pos[2]+neighbour[2])
        if new_pos in all_pos:
                    count += 1
        else:
            testing_set = set(new_pos)
            if searchingExit(new_pos,testing_set)==True:
                outside.update(testing_set)
            else:
                print(pos)
                inside.update(testing_set)
                count += 1
   
    
        
surface = 6*len(all_pos)-count
print(all_pos)
print(surface)

