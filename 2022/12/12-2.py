input = open("2022/12/Input.txt","r")
output = open("2022/12/Output.txt","w")
lines = input.read().splitlines()
Starting_Point = [0,0]
Ending_Points = []
heatmap = [[0 for i in range(len(lines[0]))] for j in range(len(lines))]
heat = 1

def printHeatmap(list):
    print("===")
    for line in list:
        print(line)
    print("===")

def canClimb(current,next):
    if ord(lines[int(current[1])][int(current[0])])-1<=ord(lines[int(next[1])][int(next[0])]):
        return True
    else:
        return False

def pathFinder(current,i):
    try:
        a = i

        if current[0]+1 < len(lines[0]):
            if canClimb(current,[current[0]+1,current[1]])==True:
                if heatmap[current[1]][current[0]+1]==0 or i<int(heatmap[current[1]][current[0]+1]):
                    current = [current[0]+1,current[1]]
                    heatmap[current[1]][current[0]]=i
                    pathFinder(current,i+1)
                    current = [current[0]-1,current[1]]
        if current[1]+1 < len(lines):
            if canClimb(current,[current[0],current[1]+1])==True:
                if heatmap[current[1]+1][current[0]]==0 or i<int(heatmap[current[1]+1][current[0]]):
                    current = [current[0],current[1]+1]
                    heatmap[current[1]][current[0]]=i
                    pathFinder(current,i+1)
                    current = [current[0],current[1]-1]
        if current[0]-1 >= 0:
            if canClimb(current,[current[0]-1,current[1]])==True:
                if heatmap[current[1]][current[0]-1]==0 or i<int(heatmap[current[1]][current[0]-1]):
                    current = [current[0]-1,current[1]]
                    heatmap[current[1]][current[0]]=i
                    pathFinder(current,i+1)
                    current = [current[0]+1,current[1]]     
        if current[1]-1 >= 0:
            if canClimb(current,[current[0],current[1]-1])==True:
                if heatmap[current[1]-1][current[0]]==0 or i<int(heatmap[current[1]-1][current[0]]):
                    current = [current[0],current[1]-1]
                    heatmap[current[1]][current[0]]=i
                    pathFinder(current,i+1)
                    current = [current[0],current[1]+1]
    except RecursionError:
        pass
        
    

for i in range(len(lines)):
    for j in range(len(lines[0])):
        if lines[i][j]=="E":
            Starting_Point = [j,i]
            lines[i] = lines[i][:j] + "z" + lines[i][j+1:]
        if lines[i][j]=="a":
            Ending_Points.append([j,i])

heatmap[Starting_Point[1]][Starting_Point[0]] = -1


pathFinder(Starting_Point,heat)
for line in heatmap:
    output.write(str(line))
    output.write("\n")
min = 900
for Ending_Point in Ending_Points:
    if heatmap[Ending_Point[1]][Ending_Point[0]] < min and heatmap[Ending_Point[1]][Ending_Point[0]]!=0:
        min = heatmap[Ending_Point[1]][Ending_Point[0]]

print("Part 2:",min,"steps are required")
