input = open("2022/10/Input.txt","r")
lines = input.read().splitlines()
global cycle
part = 1
signals = []

def printCBT(list):
    for chr in list:
        if chr == "/":
            print("",end="\n")
        else:
            print(chr,end="")

def signal_strength(x,list):
    global cycle
    if part == 1:
        if cycle%40==20:
            x = x*cycle
            list.append(x)
    elif part == 2:
        if cycle-1==x or cycle-1==x-1 or cycle-1==x+1:
            list.append("#")
        else:
            list.append(".")
        if cycle%40==0:
            cycle = 0
            list.append("/")

for i in range(2):
    cycle = 0
    x = 1
    for line in lines:
        line = line.split(" ")
        if line[0]=="noop":
            cycle += 1
            signal_strength(x,signals)
        elif line[0]=="addx":
            cycle += 1
            signal_strength(x,signals)
            cycle += 1
            signal_strength(x,signals)
            x += int(line[1])
    if part==1:
        part += 1
        print("Part 1 : The sum of the six signal strengths is",sum(signals))
        signals = []

print("Part 2 : RZEKEFHA")
printCBT(signals)



