input = open("2022/13/Input.txt","r")
lines = input.read().splitlines()
max_length = (len(lines)-2)//3
x = 0
y = 1
Packets = []

def compare(left,right):
    if type(left)==list and type(right)==int:
        return compare(left,[right])
    if type(left)==int and type(right)==list:
        return compare([left],right)
    if left==[] and right!=[]:
        return -1
    if right==[] and left!=[]:
        return 1
    if left==[] and right==[]:
        return 0
    if type(left)==int and type(right)==int:
        if left<right:
            return -1
        if left==right:
            return 0
        if left>right:
            return 1
    if type(left)==list and type(right)==list:
        while compare(left[0],right[0])==0:
            left = left[1:]
            right = right[1:]
            if left == [] or right == []:
                return compare(left,right)
        return compare(left[0],right[0])
    raise "C'est pas normal"

for i in range(max_length+1):
    left = eval(lines[i*3])
    right = eval(lines[(i*3)+1])
    if compare(left,right) == -1:
        x += i+1

for i in range(max_length+1):
    Packets.append(eval(lines[i*3]))
    Packets.append(eval(lines[(i*3)+1]))

Sorted_Packets = []
Packets = Packets + [[[2]],[[6]]]

for pack in Packets:
    Sorted_Packets.append(pack)
    for i in range(1,len(Sorted_Packets)):
        if compare(Sorted_Packets[-i],Sorted_Packets[-i-1])==-1:
            Sorted_Packets[-i],Sorted_Packets[-i-1]=Sorted_Packets[-i-1],Sorted_Packets[-i]
            
k=1
for pack in Sorted_Packets:
    if pack == [[2]] or pack == [[6]]:
        y *= k
    k+=1
    
print("Part 1 : The sum of the indices of the pairs in the right order is",x)
print("Part 2 : The decoder key is",y)
            