input = open("2022/17/Input.txt", "r")
lines = input.read().splitlines()
global gas_round
gas_round = 0
max_heights = [0 for x in range(7)]
rock_location = set()
rocks = [[(2,3),(3,3),(4,3),(5,3)],[(3,5),(2,4),(3,4),(4,4),(3,3)],[(4,5),(4,4),(2,3),(3,3),(4,3)],[(2,3),(2,4),(2,5),(2,6)],[(2,3),(3,3),(2,4),(3,4)]]

'''
          #        #     #
####     ###       #     #      ##
          #      ###     #      ##
                         #
'''

for line in lines:
    commands = line

def printRock(rock):
    for y in range(max_global_height,min_global_height-2,-1):
        print("|",end="")
        for x in range(7):
            pixel = (x,y)
            if pixel in rock:
                print("#",end="")
            else:
                print(".",end="")
        if y%5==4:
            print("| ",y+1)
        elif y == min_global_height-1:
            print("| ",y+1)
        else:
            print("|    .")
    print("---------")
    print("\n") 

def hotGasApplied(rock):
    global gas_round
    outside = False
    gas_command = commands[gas_round%len(commands)]
    new_rock = []
    if gas_command == ">":
        for pixel in rock:
            if pixel[0]+1 >= 7 or (pixel[0]+1,pixel[1]) in rock_location:
                outside = True
                break
        if outside == False:
            for pixel in rock:
                pixel = (pixel[0]+1,pixel[1])
                new_rock.append(pixel)
    elif gas_command == "<":
        for pixel in rock:
            if pixel[0]-1 < 0 or (pixel[0]-1,pixel[1]) in rock_location:
                outside = True
                break
        if outside == False:
            for pixel in rock:
                pixel = (pixel[0]-1,pixel[1])
                new_rock.append(pixel)
    if outside == True:
        new_rock = rock.copy()
    gas_round += 1
    return new_rock

def gravityApplied(rock):
    too_low = False
    for pixel in rock:
        column = pixel[0]
        if (pixel[0],pixel[1]-1) in rock_location or pixel[1]==0:
            too_low = True
            break
    if too_low == False:
        new_rock = []
        for pixel in rock:
            pixel = (pixel[0],pixel[1]-1)
            new_rock.append(pixel)
    else:
        new_rock = rock.copy()
    return new_rock

directionOrder = [(-1,0),(0,-1),(1,0),(0,1)]

def nextRock(rock,direction,orl,origins):
    '''
    
    (new_rock,new_direction)
    
    '''
    dir_i = directionOrder.index(direction)
    for i in range(3):
        check_direction = directionOrder[(dir_i+i-1)%4]
        new_rock = (rock[0] + check_direction[0],rock[1] + check_direction[1])
        if new_rock in rock_location and new_rock not in orl:
            orl.add(new_rock)
            origins[new_rock] = rock
            return (new_rock,check_direction)
    new_rock = origins[rock]
    new_direction = (new_rock[0]-rock[0],new_rock[1]-rock[1])
    return (new_rock,new_direction) 
            
    
 
def usefullRocks():
    orl = set()
    origins = {}
    rock = (6,max_heights[6]-1)
    orl.add(rock)
    direction = (-1,0)
    last_rock = (0,max_heights[0]-1)
    while last_rock not in origins or rock != origins[last_rock]:
        (rock,direction)=nextRock(rock,direction,orl,origins)
    return orl 

part = 2022

for j in range(2):
    max_global_height = 0
    min_global_height = 0
    all_rock_locations = {}
    gas_round = 0
    max_heights = [0 for x in range(7)]
    rock_location = set()
    for i in range(100000):
        rock = []
        new_rock = rocks[i%5]
        for stone in new_rock:
            stone = (stone[0],stone[1]+max_global_height)
            rock.append(stone)
        while True:
            n_rock = hotGasApplied(rock)
            rock = gravityApplied(n_rock)
            if n_rock==rock:
                break
        for pixel in rock:
            rock_location.add(pixel)
            column = pixel[0]
            if pixel[1]+1 > max_heights[column]:
                max_heights[column] = pixel[1]+1
        max_global_height = max(max_heights)
        min_global_height = min(max_heights)
        if i > 5:
            rock_location = usefullRocks()  
        rock_location_list = []
        for rock in rock_location:
            rock = (rock[0],rock[1]-max_global_height)
            rock_location_list.append(rock)
        rock_location_list.sort()
        key = (str(rock_location_list),i%5,gas_round%len(commands))
        if key in all_rock_locations:
            (first_height,first_i) = all_rock_locations[key]
            loop_i = i-first_i
            loop_height = max_global_height-first_height
            rest = ((part-first_i)%loop_i)-1
            for (height_value,i_value) in all_rock_locations.values():
                if i_value == first_i + rest:
                    last_height = height_value-first_height
                    break      
            max_global_height = first_height + loop_height*((part-first_i)//loop_i) + last_height
            break
        else:
            all_rock_locations[key] = (max_global_height,i)
    print("Part",j+1,": The maximum height is :",max_global_height)
    part = 1000000000000
            
        



    

