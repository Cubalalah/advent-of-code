input = open("2022/15/Input.txt","r")
lines = input.read().splitlines()
Sensors = []

for line in lines:
    line = line.split(" ")
    sensor_x = int(line[2][2:-1])
    sensor_y = int(line[3][2:-1])
    beacon_x = int(line[8][2:-1])
    beacon_y = int(line[9][2:])
    distance = abs(beacon_x-sensor_x)+abs(beacon_y-sensor_y)
    Sensors.append([sensor_x,sensor_y,distance])
    
min_x = 0
max_x = 4000000
max_y = max_x


def checkingInterval(y):
    if y >= max_y:
        return None
    for interval in Intervals:
        if interval[0]<=y+1 and interval[1]>y:
            return checkingInterval(interval[1])
    return y+1



for x in range(min_x,max_x+1):
    Intervals = []
    for sensor in Sensors:
        if abs(sensor[0]-x)<=sensor[2]:
            Intervals.append([sensor[1]-(sensor[2]-abs(sensor[0]-x)),sensor[1]+(sensor[2]-abs(sensor[0]-x))])
    y = checkingInterval(-1)
    if y != None:
        print("Part 2 : The tuning frequency is :",x*4000000+y)
        break
        

            
