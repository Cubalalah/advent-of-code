input = open("2022/15/Input.txt","r")
lines = input.read().splitlines()
Sensors = []
Sensors_Test = []
Beacons = []

for line in lines:
    line = line.split(" ")
    sensor_x = int(line[2][2:-1])
    sensor_y = int(line[3][2:-1])
    beacon_x = int(line[8][2:-1])
    beacon_y = int(line[9][2:])
    distance = abs(beacon_x-sensor_x)+abs(beacon_y-sensor_y)
    Sensors.append([sensor_x,sensor_y,distance])
    Sensors_Test.append([sensor_x,sensor_y])
    Beacons.append([beacon_x,beacon_y])

min_x = min(sensor[0]-sensor[2] for sensor in Sensors)
max_x = max(sensor[0]+sensor[2] for sensor in Sensors)

y = 2000000
visible = 0

for x in range (min_x,max_x+1):
    for sensor in Sensors:
        if [x,y] in Beacons:
            break
        if [x,y] in Sensors_Test:
            break
        if abs(x-sensor[0])+abs(y-sensor[1])<=sensor[2]:
            visible += 1
            break

print("Part 1 :",visible," positions cannot contain a beacon")
