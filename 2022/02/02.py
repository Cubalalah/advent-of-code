input = open("2022/02/Input.txt","r")
lines = input.readlines()
score=0

# A,X = Rock
# B,Y = Paper
# C,Z = Scissors

def tictactoe_part1(you,me):                          # Search for the output of the game
    if me==you:                                         # Draw (A-A)(B-B)(C-C)
        return 3                                        
    if ord(you)-ord(me)==1 or ord(you)-ord(me)==-2:     # Loss (B-A)(C-B)(A-C)
        return 0
    else:                                               # Win  (A-B)(B-C)(C-A)
        return 6

# X = Loss
# Y = Draw
# Z = Win

def tictactoe_part2(you,output):    # Search for the move to play for the chosen output
    me=0
    if output=="Y":                # Draw 
        me=you
    if output=="Z":                # Win
        if you!="C":                # With A and B, the winning choice is letter+1 (A+1 = B)(B+1 = C)
            me=chr(ord(you)+1)
        else:                       # With C, it's letter-2 (C-2 = A)
            me="A"
    if output=="X":                # Loss
        if you!="A":                # With B and C, the loosing choice is letter-1 (C-1 = B)(B-1 = A)
            me=chr(ord(you)-1)
        else:                       # With A, it's letter+2 (C+2 = A)
            me="C"
    return(ord(str(me))-64)        # Conversion in score (A=1,B=2,C=3)



for line in lines:
    if line[2]=="X":                # Rock (+1)
        score = score+1             
    elif line[2]=="Y":              # Paper (+2)
        score = score+2             
    elif line[2]=="Z":              # Scissors (+3)
        score = score+3
    score = score+tictactoe_part1(line[0],chr(ord(str(line[2]))-23))    # Search for the output of the tictactoe game + Letter conversion (X=A,Y=B,Z=C)

print("Part 1 : My total score would be",score)

score=0
for line in lines:
    if line[2]=="X":                # Loss (+0)
        score = score+0
    elif line[2]=="Y":              # Draw (+3)
        score = score+3
    elif line[2]=="Z":              # Win (+6)
        score = score+6
    score = score+tictactoe_part2(line[0],line[2])                      # Search for the move to play for the chosen output

print("Part 2 : My total score would be",score)