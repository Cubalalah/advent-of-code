input = open("2022/06/Input.txt","r")
lines = input.read().splitlines()
data_stream_buffer = lines[0]
number_of_distinct_characters = 0

def equal(i,num):
    test = data_stream_buffer[i:i+num]
    for i in range(97,123):
        if test.count(chr(i))>1:
            return 1
        
number_of_distinct_characters = 4
for i in range(len(data_stream_buffer)-number_of_distinct_characters-1):
    if equal(i,number_of_distinct_characters) != 1 :
        print("Part 1 :",i+number_of_distinct_characters,"caracters need to be processed before the first-to-packet marker with 4 different characters")
        break
number_of_distinct_characters = 14
for i in range(len(data_stream_buffer)-number_of_distinct_characters-1):
    if equal(i,number_of_distinct_characters) != 1 :
        print("Part 2 :",i+number_of_distinct_characters,"caracters need to be processed before the first-to-packet marker with 14 different characters")
        break


