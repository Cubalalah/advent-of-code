input = open("2022/07/Input.txt","r")
lines = input.read().splitlines()
filesystem = {"/main":[]}                           # Dictionnary with all the directories (each directory is identified as its path)
path = "/main"                                      # At the beginning you are in one big directory, the "main" directory                            

def size(directory):                                # Fonction that calculate the size of a directory
    sum = 0
    for file in filesystem[directory]:
        try:                                        # If file can't be an integer, then it's a string => it's the name of another directory
            sum += int(file)
        except ValueError:
            new_directory = directory + "/" + file  # Setting the name of the new directory with the path
            sum += size(new_directory)              # Recursivity
    return(sum)


for line in lines :                            
    line = line.split(" ")                          # Split the line with the spaces => Create a list, easier to manipulate
    if line[0] == "$":  
        if line[1] == "cd":
            if line[2] == "/":
                path = "/main"
            elif line[2] == "..":
                path = path[:path.rfind("/")]       # If ".." then you have to move out one level, so you suppr the end of the string unless there is a "/"     
            else:
                path = path + "/" + line[2]         
                try :                                       
                    filesystem[path]=filesystem[path]   # If there's an error, the path did not exist in filesystem, so we have to create it
                except KeyError:
                    filesystem[path]=[]             # Creation of the path in the directory as an empty list
    elif line[0]=="dir":
        filesystem[path].append(line[1])            # Append the name of the directory if the line begins with "dir"
    else:
        filesystem[path].append(line[0])            # Else, append with the size of the file


count = 0

for directory in filesystem:
    if size(directory)<=100000:                     # Part 1 : Sum of all the sirectories with a size below 100000
        count += size(directory)

print("Part 1 : The sum of all directories with a size below 100000 is :",count)


directory_needed = 70000000
space_needed = 30000000-(70000000-size("/main"))    # Calculation of the space thats needs to be suppr

for directory in filesystem:
    if size(directory)>=space_needed:
        if int(size(directory)) < int(directory_needed):       # Part 2 : Find the smallest directory, which is large enough to empty the space needed
            directory_needed = size(directory)

print("Part 2 : The size of the directory that we need to delete is :",directory_needed)