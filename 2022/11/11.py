import sys
input = open("2022/11/Input.txt","r")
lines = input.read().splitlines()

def Monkey(item,divisible,operation,number,true,false,monkey):
    Monkeys[monkey] += 1
    if operation == "*":
        item = int(item)*int(number)
    elif operation == "+":
        item = int(item)+int(number)
    if part == 1:
        item = int(item)// 3
    elif part == 2:
        item = item % (2*3*5*7*11*13*17*19)
    if item%divisible == 0:
        return(item,true)
    else:
        return(item,false)

for part in range(1,3):
    starting_items = []
    Monkeys = [0,0,0,0,0,0,0,0]
    temp = 0
    for k in range(20+9980*(part-1)):
        for i in range(8):
            starting_items = lines[7*i+1].split(" ")[4:]
            for item in starting_items:
                if item[-1] == ",":
                    item = item[:-1]
                if lines[7*i+2].split(" ")[7] == "old":
                    number=item
                else:
                    number=lines[7*i+2].split(" ")[7]
                temp = Monkey(int(item),int(lines[7*i+3].split(" ")[5]),lines[7*i+2].split(" ")[6],number,int(lines[7*i+4].split(" ")[9]),int(lines[7*i+5].split(" ")[9]),int(lines[7*i].split(" ")[1][:-1]))
                test1 = lines[7*i+1].index(":")
                try :
                    test2 = lines[7*i+1].index(",")
                    if test2 == test1+1:
                        lines[7*i+1] = lines[7*i+1][:test2] + lines[7*i+1][test2+1:]
                        test2 = lines[7*i+1].index(",")
                    lines[7*i+1] = lines[7*i+1][:test1] + ":" + lines[7*i+1][test2+1:]
                except ValueError:
                    lines[7*i+1] = lines[7*i+1][:test1] + ":"
                lines[7*int(temp[1])+1] = lines[7*int(temp[1])+1] + ", " + str(temp[0])

    print ("Part",part,": The level of monkey business is :",sorted(Monkeys)[-1]*sorted(Monkeys)[-2])


