input = open("2022/09/Input.txt","r")
lines = input.read().splitlines()

Tail_visited_places = [[0,0]]
tail_location = [0,0]
head_location = [0,0]

def WhereIsTail(head_location,tail_location,X,Y):
    if abs(tail_location[0]-head_location[0]) < 2 and abs(tail_location[1]-head_location[1]) < 2:
        return(tail_location) 
    else:
        if X >= 1:                      # X = Head(x)-Tail(x) :
            tail_location[0] += 1                       # if X > 1 : Tail at the left of Head and have to move (go right)
        if X <= -1:                                     # if X < 1 : Tail at the right of Head and have to move (go left)
            tail_location[0] -= 1                       
        if Y >= 1:                      # Y = Head(y)-Tail(y):
            tail_location[1] += 1                       # if Y > 1 : Tail at the bottom of Head and have to move (go up)
        if Y <= -1:                                     # if Y < 1 : Tail at the top of Head and have to move (go down)
            tail_location[1] -= 1
        return(tail_location)

def One_Tail(head_location,tail_location,X,Y):
    tail_location = WhereIsTail(head_location,tail_location,X,Y).copy()
    if tail_location not in Tail_visited_places:
        Tail_visited_places.append(tail_location)

for line in lines:
    line = line.split(" ")
    if line[0] == "R":
        for i in range(int(line[1])):
            head_location[0] += 1
            One_Tail(head_location,tail_location,head_location[0]-tail_location[0],head_location[1]-tail_location[1])
    elif line[0] == "L":
        for i in range(int(line[1])):
            head_location[0] -= 1
            One_Tail(head_location,tail_location,head_location[0]-tail_location[0],head_location[1]-tail_location[1])
    elif line[0] == "U":
        for i in range(int(line[1])):
            head_location[1] += 1
            One_Tail(head_location,tail_location,head_location[0]-tail_location[0],head_location[1]-tail_location[1])
    elif line[0] == "D":
        for i in range(int(line[1])):
            head_location[1] -= 1
            One_Tail(head_location,tail_location,head_location[0]-tail_location[0],head_location[1]-tail_location[1])

print("Part 1 : The tail of the rope visit at least once", len(Tail_visited_places),"positions")




