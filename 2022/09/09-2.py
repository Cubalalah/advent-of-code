input = open("2022/09/Input.txt","r")
lines = input.read().splitlines()

Last_tail_visited_places = [[0,0]]
tails_location = [[0,0] for i in range(10)]

def WhereIsTail(head_location,tail_location,X,Y):
    if abs(tail_location[0]-head_location[0]) < 2 and abs(tail_location[1]-head_location[1]) < 2:
        return(tail_location) 
    else:
        if X >= 1:                      # X = Tail(i-1)(x)-Tail(i)(x) :
            tail_location[0] += 1                       # if X > 1 : Tail at the left of previous Tail and have to move (go right)
        if X <= -1:                                     # if X < 1 : Tail at the right of previous Tail and have to move (go left)
            tail_location[0] -= 1                       
        if Y >= 1:                      # Y = Tail(i-1)(y)-Tail(i)(y):
            tail_location[1] += 1                       # if Y > 1 : Tail at the bottom of previous Tail and have to move (go up)
        if Y <= -1:                                     # if Y < 1 : Tail at the top of previous Tail and have to move (go down)
            tail_location[1] -= 1
        return(tail_location)

def Tail(head_location,tail_location,X,Y):
    tail_location = WhereIsTail(head_location,tail_location,X,Y)

def Last_Tail(head_location,tail_location,X,Y):
    tail_location = WhereIsTail(head_location,tail_location,X,Y)
    tail_location_copy = tail_location.copy()
    if tail_location_copy not in Last_tail_visited_places:
        Last_tail_visited_places.append(tail_location_copy)

for line in lines:
    line = line.split(" ")
    if line[0] == "R":
        for i in range(int(line[1])):
            tails_location[0][0] += 1
            for i in range(1,9):
                Tail(tails_location[i-1],tails_location[i],tails_location[i-1][0]-tails_location[i][0],tails_location[i-1][1]-tails_location[i][1])
            Last_Tail(tails_location[8],tails_location[9],tails_location[8][0]-tails_location[9][0],tails_location[8][1]-tails_location[9][1])
    elif line[0] == "L":
        for i in range(int(line[1])):
            tails_location[0][0] -= 1
            for i in range(1,9):
                Tail(tails_location[i-1],tails_location[i],tails_location[i-1][0]-tails_location[i][0],tails_location[i-1][1]-tails_location[i][1])
            Last_Tail(tails_location[8],tails_location[9],tails_location[8][0]-tails_location[9][0],tails_location[8][1]-tails_location[9][1])
    elif line[0] == "U":
        for i in range(int(line[1])):
            tails_location[0][1] += 1
            for i in range(1,9):
                Tail(tails_location[i-1],tails_location[i],tails_location[i-1][0]-tails_location[i][0],tails_location[i-1][1]-tails_location[i][1])
            Last_Tail(tails_location[8],tails_location[9],tails_location[8][0]-tails_location[9][0],tails_location[8][1]-tails_location[9][1])
    elif line[0] == "D":
        for i in range(int(line[1])):
            tails_location[0][1] -= 1
            for i in range(1,9):
                Tail(tails_location[i-1],tails_location[i],tails_location[i-1][0]-tails_location[i][0],tails_location[i-1][1]-tails_location[i][1])
            Last_Tail(tails_location[8],tails_location[9],tails_location[8][0]-tails_location[9][0],tails_location[8][1]-tails_location[9][1])

print("Part 2 : The tail of the rope visit at least once", len(Last_tail_visited_places),"positions")
