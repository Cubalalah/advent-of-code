import typing
from collections import deque

input = open("2022/16/Input.txt", "r")
lines = input.read().splitlines()

def calculate_distances(start_node: str):
    node_distances[start_node]={}
    queue = deque()
    queue.append((start_node, 0))
    already_seen = set()
    already_seen.add(start_node)
    while len(queue) != 0:
        (current_node, distance) = queue.popleft()
        for neighbour in valves[current_node].tunnels:
            if neighbour not in already_seen:
                queue.append((neighbour, distance+1))
                already_seen.add(neighbour)
                if valves[neighbour].pressure != 0:
                    node_distances[start_node][neighbour] = distance+1

class Valve:
    def __init__(self, name: str, pressure: int, tunnels: list[str]):
        self.pressure = pressure
        self.tunnels = tunnels
        self.name = name

node_distances: dict[str, dict[str, int]] = {}

valves: dict[str, Valve] = {}

for line in lines:
    LeadingTo = []
    line = line.split(" ")
    for i in range(9, len(line)-1):
        LeadingTo.append(line[i][:-1])
    LeadingTo.append(line[len(line)-1])
    valves[line[1]] = Valve(line[1], int(line[4][5:-1]), LeadingTo)

for node in valves.keys():
    if valves[node].pressure != 0 or node == "AA":
        calculate_distances(node)

maxi = 0
count = 0

def node_calculation(next_node,distance,minute,pr,tpr,already_opened):
    if next_node not in already_opened and minute+distance<26:
        new_minute = minute + distance + 1
        new_tpr = tpr + pr * (distance + 1)
        new_pr = pr + valves[next_node].pressure
        new_already_opened = already_opened.copy()
        new_already_opened.add(next_node)
        return new_minute,new_pr,new_tpr,new_already_opened
    else:
        return 0

def final_calculation(current_human_node,current_elephant_node,human_minute,elephant_minute,human_pr,elephant_pr,human_tpr,elephant_tpr,already_opened,stop_elephant,stop_human):
    
    global maxi
    global count
    
    if stop_human and stop_elephant:
        count += 1
        if human_minute<26:
            human_tpr += human_pr * (26-human_minute)
        if elephant_minute<26:
            elephant_tpr += elephant_pr * (26-elephant_minute)
        tpr = human_tpr + elephant_tpr
        if tpr > maxi:
            maxi = tpr
        if count%10000==0:
            print(maxi)
            
    elif (human_minute>elephant_minute or stop_human) and not stop_elephant:
        stop_elephant = True
        for (next_elephant_node,distance) in node_distances[current_elephant_node].items():
            elephant = node_calculation(next_elephant_node,distance,elephant_minute,elephant_pr,elephant_tpr,already_opened)
            if elephant != 0:
                stop_elephant = False
                final_calculation(current_human_node,next_elephant_node,human_minute,elephant[0],human_pr,elephant[1],human_tpr,elephant[2],elephant[3],stop_elephant,stop_human)
        if stop_elephant:
            final_calculation(current_human_node,current_elephant_node,human_minute,elephant_minute,human_pr,elephant_pr,human_tpr,elephant_tpr,already_opened,stop_elephant,stop_human)
   
    elif (human_minute<=elephant_minute or stop_elephant) and not stop_human:
        stop_human = True
        for (next_human_node,distance) in node_distances[current_human_node].items():
            human = node_calculation(next_human_node,distance,human_minute,human_pr,human_tpr,already_opened)
            if human != 0:
                stop_human = False
                final_calculation(next_human_node,current_elephant_node,human[0],elephant_minute,human[1],elephant_pr,human[2],elephant_tpr,human[3],stop_elephant,stop_human)
        if stop_human:
            final_calculation(current_human_node,current_elephant_node,human_minute,elephant_minute,human_pr,elephant_pr,human_tpr,elephant_tpr,already_opened,stop_elephant,stop_human) 


print("Wait a little to have a stabilized result")
final_calculation("AA","AA",0,0,0,0,0,0,set(),False,False)
