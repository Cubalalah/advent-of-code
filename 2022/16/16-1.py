import typing
from collections import deque

input = open("2022/16/Input.txt", "r")
lines = input.read().splitlines()


class Valve:
    def __init__(self, name: str, pressure: int, tunnels: list[str]):
        self.pressure = pressure
        self.tunnels = tunnels
        self.name = name


node_distances: dict[str, dict[str, int]] = {}


def calculate_distances(start_node: str):
    node_distances[start_node]={}
    queue = deque()
    queue.append((start_node, 0))
    already_seen = set()
    already_seen.add(start_node)
    while len(queue) != 0:
        (current_node, distance) = queue.popleft()
        for neighbour in valves[current_node].tunnels:
            if neighbour not in already_seen:
                queue.append((neighbour, distance+1))
                already_seen.add(neighbour)
                if valves[neighbour].pressure != 0:
                    node_distances[start_node][neighbour] = distance+1


pressure_released = 0
total_pressure_released = 0
list = []
stop = False

def node_path(node, minute, pressure_released, total_pressure_released, already_opened):
    results = []
    for (next_node,distance) in node_distances[node].items():
        if next_node not in already_opened:
            if minute + distance < 26:
                new_minute = minute + distance + 1
                new_total_pressure_released = total_pressure_released + pressure_released * (distance+1)
                new_pressure_released = pressure_released + valves[next_node].pressure
                new_already_opened = already_opened.copy()
                new_already_opened.add(next_node)
                results.append(node_path(next_node, new_minute, new_pressure_released ,new_total_pressure_released ,new_already_opened))
    if results == []:
        results.append(total_pressure_released + pressure_released * (26-minute))
    return max(results)
    

valves: dict[str, Valve] = {}

for line in lines:
    LeadingTo = []
    line = line.split(" ")
    for i in range(9, len(line)-1):
        LeadingTo.append(line[i][:-1])
    LeadingTo.append(line[len(line)-1])
    valves[line[1]] = Valve(line[1], int(line[4][5:-1]), LeadingTo)

for node in valves.keys():
    if valves[node].pressure != 0 or node == "AA":
        calculate_distances(node)
print(node_distances)

node = "AA"

print(node_path("AA", 0, 0, 0,set()))

