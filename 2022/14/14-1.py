input = open("2022/14/Input.txt","r")
lines = input.read().splitlines()
map = set()
sand = set()

def fallingSand(pos):
    if (pos[0],pos[1]+1) in map or (pos[0],pos[1]+1) in sand:
        if (pos[0]-1,pos[1]+1) in map or (pos[0]-1,pos[1]+1) in sand:
            if (pos[0]+1,pos[1]+1) in map or (pos[0]+1,pos[1]+1) in sand:
                sand.add(pos)
                return 1
            else:
                return fallingSand((pos[0]+1,pos[1]+1))
        else:
            return fallingSand((pos[0]-1,pos[1]+1))
    else:
        return fallingSand((pos[0],pos[1]+1))


for line in lines:
    line = [[int(x) for x in pair.split(",")] for pair in line.split(" -> ")]
    for i in range(len(line)-1):
        if line[i][0]==line[i+1][0]:
            a=1
            if line[i][1]>line[i+1][1]:
                a=-1
            for j in range (line[i][1],line[i+1][1]+a,a):
                map.add((line[i][0],j))
        elif line[i][1]==line[i+1][1]:
            a=1
            if line[i][0]>line[i+1][0]:
                a=-1
            for j in range (line[i][0],line[i+1][0]+a,a):
                map.add((j,line[i][1]))


map.add((499,0))
min_x = min(x for (x,_) in map)
min_y = min(x for (_,x) in map)
max_x = max(x for (x,_) in map)
max_y = max(x for (_,x) in map)



i = 0
while True:
    try :
        fallingSand((500,0))
        i += 1
    except RecursionError:
        break

for y in range(min_y,max_y+1):
    for x in range(min_x,max_x+1):
        if (x,y) in map:
            print("#",end='')
        elif (x,y) in sand:
            print("o",end='')
        else:
            print(".",end='')
    print()
    
print("Part 1 :",i,"units of sand are necessary.")