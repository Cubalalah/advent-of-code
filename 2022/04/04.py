input = open("2022/04/Input.txt","r")
lines = input.read().splitlines()
pairs=[]

fully_contain,overlap=0,0
for line in lines:
    pairs.append(line.split(","))

for pair in pairs:
    assignment_1=pair[0].split("-")
    assignment_2=pair[1].split("-")
    if int(assignment_1[0])<=int(assignment_2[0]) and int(assignment_1[1])>=int(assignment_2[1]) or int(assignment_1[0])>=int(assignment_2[0]) and int(assignment_1[1])<=int(assignment_2[1]):
        fully_contain += 1
    if int(assignment_1[0])>=int(assignment_2[0]) and int(assignment_1[0])<=int(assignment_2[1]) or int(assignment_1[0])<=int(assignment_2[0]) and int(assignment_1[1])>=int(assignment_2[0]):
        overlap += 1

print("Part 1 :",fully_contain,"assignments pairs does fully contain each other")
print("Part 2 :",overlap,"assignments pairs does overlap each other")
