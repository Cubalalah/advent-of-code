test = open("2022/01/Input.txt", "r").read().splitlines()
elf = 0
max = 0
Elf_list=[]

for food in test:
    if food == "":
        if elf > max:
            max=elf
        Elf_list.append(elf)
        elf=0
    else:
        food = int(food)
        elf += food

Elf_list.sort()
top3=Elf_list[-3:]

print("Part 1 : The Elf carrying the most calories is carrying", max,"calories")
print("Part 2 : The top three Elves carrying the most calories is carrying", top3[1]+top3[2]+top3[0],"calories")