input = open("2022/08/Input.txt","r")
lines = input.read().splitlines()
columns = []

def visible(column,column_index,line,line_index):
    visible = 0
    if column_index == 0 or line_index == 0 or column_index == len(column)-1 or line_index == len(line)-1:
        return 1
    for chr in line[:line_index]:
        if chr >= line[line_index]:
            visible += 1
            break
    for chr in line[line_index+1:]:
        if chr >= line[line_index]:
            visible += 1
            break
    for chr in column[:column_index]:
        if chr >= column[column_index]:
            visible += 1
            break
    for chr in column[column_index+1:]:
        if chr >= column[column_index]:
            visible += 1
            break
    if visible < 4:
        return 1
    else:
        return 0

def scenic_score(column,column_index,line,line_index):
    score = 1
    i = 1
    if column_index == 0 or line_index == 0 or column_index == len(column)-1 or line_index == len(line)-1:
        return 0
    for chr in line[:line_index][::-1]:
        if chr >= line[line_index]:
            break
        if line_index-i == 0:
            break
        i += 1
    score *= i
    i = 1
    for chr in line[line_index+1:]:
        if chr >= line[line_index]:
            break
        if 98-i == line_index:
            break
        i += 1
    score *= i
    i = 1
    for chr in column[:column_index][::-1]:
        if chr >= column[column_index]:
            break
        if column_index-i == 0:
            break
        i += 1
    score *= i
    i = 1
    for chr in column[column_index+1:]:
        if chr >= column[column_index]:
            break
        if 98-i == column_index:
            break
        i += 1
    score *= i
    return(score)

for i in range(len(lines[0])):              # Creation of a list with the columns
    column = ""
    for j in range(len(lines)):
        line = lines[j]
        column = column + str(line[i])
    columns.append(column)

count = 0
score = 0

for i in range(len(lines)):
    for j in range(len(lines[i])):
        count += visible(columns[j],i,lines[i],j)
        if scenic_score(columns[j],i,lines[i],j) > score:
            score = scenic_score(columns[j],i,lines[i],j)

print("Part 1 :",count,"trees are visible from outside")
print("Part 2 : The best tree has a scenic score of",score)



