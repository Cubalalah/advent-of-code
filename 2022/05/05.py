input = open("2022/05/Input.txt","r")
lines = input.read().splitlines()
columns = [[] for i in range(9)]
CrateMover_version = 9000

def space_deletion():
    u=0
    for column in columns:
        try:
            column.remove(" ")
        except ValueError:
            u=u+1

def column_display():
    maxi = max(len(columns[0]),len(columns[1]),len(columns[2]),len(columns[3]),len(columns[4]),len(columns[5]),len(columns[6]),len(columns[7]),len(columns[8]))
    for i in range(9):
        for j in range(maxi+1):
            try :
                columns[i][j] = columns[i][j]
            except IndexError:
                columns[i].append(" ")
    print("-------------------------------------")
    print("Part",CrateMover_version-9000)
    print("-------------------------------------")
    for i in range(max(len(columns[0]),len(columns[1]),len(columns[2]),len(columns[3]),len(columns[4]),len(columns[5]),len(columns[6]),len(columns[7]),len(columns[8]))-1,-1,-1):
        print(f" [{columns[0][i]}] [{columns[1][i]}] [{columns[2][i]}] [{columns[3][i]}] [{columns[4][i]}] [{columns[5][i]}] [{columns[6][i]}] [{columns[7][i]}] [{columns[8][i]}] ")

while CrateMover_version < 9002 :
    x = 0
    for i in range(7,-1,-1):
        height = str(lines[i])
        columns[0].append(height[1])
        columns[1].append(height[5])
        columns[2].append(height[9])
        columns[3].append(height[13])
        columns[4].append(height[17])
        columns[5].append(height[21])
        columns[6].append(height[25])
        columns[7].append(height[29])
        columns[8].append(height[33])
        space_deletion()
        
    for line in lines:
        if "move" in line:
            space_deletion()
            instruction = line.split(" ")
            instruction.pop(4)
            instruction.pop(2)
            instruction.pop(0)
            number_of_crates,starting_point,ending_point = instruction[0],instruction[1],instruction[2]
            temp_column = columns[int(starting_point)-1][len(columns[int(starting_point)-1])-int(number_of_crates):]
            if CrateMover_version == 9000:
                temp_column = temp_column[::-1]
            columns[int(starting_point)-1] = columns[int(starting_point)-1][:-int(number_of_crates)]
            for crate in temp_column:
                columns[int(ending_point)-1].append(crate)
    CrateMover_version += 1
    column_display()


