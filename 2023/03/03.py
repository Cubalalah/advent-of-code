from string import digits
symbol = '#$%&*+-/@='
input = open("2023/03/input.txt", "r")
page = input.read().splitlines()

def tableTransformation(page):
    table = []
    for line in page:
        little_table = []
        for char in line:
            little_table.append(char)
        table.append(little_table)
    return table

def symbolFinder(page):
    symbols = []
    for y in range(len(page)):
        for x in range(len(page[0])):
            if page[y][x] in symbol:
                symbols.append((x,y,page[y][x]))
    return symbols

def numberFinder(page,x,y):
    numbers = []
    if y>0:
        if x>0:    
            if page[y-1][x-1] in digits:
                numbers.append((x-1,y-1))
        if page[y-1][x] in digits:
            numbers.append((x,y-1))
        if x<len(page[0])-1:
            if page[y-1][x+1] in digits:
                numbers.append((x+1,y-1))
    if x>0:
        if page[y][x-1] in digits:
            numbers.append((x-1,y))
    if x<len(page[0])-1:
        if page[y][x+1] in digits:
            numbers.append((x+1,y))
    if y<len(page)-1:
        if x>0:    
            if page[y+1][x-1] in digits:
                numbers.append((x-1,y+1))
        if page[y+1][x] in digits:
            numbers.append((x,y+1))
        if x<len(page[0])-1:
            if page[y+1][x+1] in digits:
                numbers.append((x+1,y+1))
    return numbers

def numberMaker(page,x,y):
    number = [x,y,x]
    if x>0:
        if page[y][x-1] in digits:
            number[0] = x-1
            if x>1:
                if page[y][x-2] in digits:
                    number[0] = x-2
    if x<len(page[0])-1:
        if page[y][x+1] in digits:
            number[2] = x+1
            if x<len(page[0])-2:
                if page[y][x+2] in digits:
                    number[2] = x+2
    return number

def gearRatioComputer(list,symbol):
    if len(list)==2 and symbol=="*":
        multiplication = 1
        for number in list:
            part_number = ""
            for i in page[number[1]][number[0]:number[2]+1]:
                part_number += i
            multiplication *= int(part_number)
        return multiplication
    return 0

def partNumbersComputer(list):
    output = 0
    for number in list:
        part_number = ""
        for i in page[number[1]][number[0]:number[2]+1]:
            part_number += i
        output += int(part_number)
    return output

numbers = []
output_1 = 0
output_2 = 0
page = tableTransformation(page)
symbols = symbolFinder(page)
for x,y,symbol in symbols:
    number_list = numberFinder(page,x,y)
    new_number_list = []
    for number in number_list:
        number = numberMaker(page,number[0],number[1])
        if number not in numbers:
            numbers.append(number)
            new_number_list.append(number)
    output_1 += partNumbersComputer(new_number_list)
    output_2 += gearRatioComputer(new_number_list,symbol)
    


print(f"Part 1 - The sum of all the part numbers is {output_1}")
print(f"Part 2 - The sum of all gear ratios is {output_2}")