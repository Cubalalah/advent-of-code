input = open("2023/02/input.txt", "r")
page = input.read().splitlines()

max_blue = 14
max_green = 13
max_red = 12
output_1 = 0
output_2 = 0

for game in page:
    cubes = {'red':0, 'green':0, 'blue':0}
    number_game = int(game.split()[1][:-1])
    game = game[game.find(":")+1:]
    game = game.split(";")
    for combination in game:
        combination = combination.split(",")
        for cube in combination:
            _, number, color = cube.split(" ")
            if cubes[color] < int(number):
                cubes[color] = int(number)
    if cubes["red"] <= max_red and cubes["blue"] <= max_blue and cubes["green"] <= max_green :           
        output_1 += number_game
    output_2 += cubes["red"]*cubes["blue"]*cubes["green"]

print(f"Part 1 - The sum of the IDs of the game is {output_1}")
print(f"Part 2 - The sum of the powers of the game is {output_2}")
            