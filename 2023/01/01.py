from string import digits


def calibrationValuesFinder(string: str):
    calibration_values = ""
    for char in string:
        if char in digits:
            calibration_values += char
            if len(calibration_values)>2:
                calibration_values = calibration_values[0] + calibration_values[2]
    if len(calibration_values)==1:
        calibration_values += calibration_values
    return int(calibration_values)

def letteredNumbersChanger(string):
    lettered_numbers = [len(string),-1,0,-1]
    written_digits = {'one':'1','two':'2','three':'3','four':'4','five':'5','six':'6','seven':'7','eight':'8','nine':'9'}
    for digit in written_digits.keys():
        if digit in string:
            pos_b = string.find(digit)
            pos_e = string.rfind(digit)
            if pos_b <= lettered_numbers[0]:
                lettered_numbers[0] = pos_b
                lettered_numbers[1] = digit
            if pos_e >= lettered_numbers[2]:
                lettered_numbers[2] = pos_e
                lettered_numbers[3] = digit
    if lettered_numbers[1] != -1 and lettered_numbers[3] != -1:
        cursed_thing = True
        for digit in string[lettered_numbers[2]:]:
            if digit in digits:
                cursed_thing = False
        if lettered_numbers[0]+len(lettered_numbers[1]) > lettered_numbers[2]:
            if cursed_thing:
                lettered_numbers[0] = lettered_numbers[2]
                lettered_numbers[1] = lettered_numbers[3]
            else:
                lettered_numbers[2] = lettered_numbers[0]
                lettered_numbers[3] = lettered_numbers[1]
        return string[:lettered_numbers[0]] + written_digits[lettered_numbers[1]] + string[lettered_numbers[0]+len(digit)-1:lettered_numbers[2]] + written_digits[lettered_numbers[3]] + string[lettered_numbers[2]+len(digit)-1:]
    return string           

input = open("2023/01/input.txt", "r")
page = input.read().splitlines()

for part in [1,2]:
    result = 0
    for line in page:
        if part == 2:
            line = letteredNumbersChanger(line)
        result += calibrationValuesFinder(line)
    print(f" Part - {part} | The sum of all the calibration values is {result}")
input.close()